"""
Bot for pinging groups of people in a Discord server.

Uses discord.py version 1.3.3, might work on other versions
Needs python >= 3.7.0 apparently
"""
import json
import discord


# Put your bot's token here
#
""" BE CAREFUL NOT TO SHARE IT WITH ANYONE!! """
#
TOKEN = ''

# Path to the file containing all the data
# THIS FILE IS IMPORTANT SO MAKE REGULAR BACKUPS
DATA_FILE_PATH = './data.json'

# List of command keywords
# Every command must start with the specified keyword
CMD = {
    'hello': 'pb!hello',
    'join_group': 'pb!join',
    'leave_group': 'pb!leave',
    'list_groups': 'pb!list',
    'send_ping': 'pb!ping',
}

# If 'dm' sends a dm to all users
# If 'mention' sends a message @mentioning every user in that ping group
DM_OR_MENTION = 'dm'


data = None
client = discord.Client()


@client.event
async def on_message(msg):
    """
    Parses the message and executes command if keyword was detected.
    Called every time a message is received.
    """
    # we do not want the bot to reply to itself
    if msg.author == client.user:
        return
    # Hello command
    # Sends a hello
    if msg.content.startswith(CMD['hello']):
        await msg.channel.send(f'Hello {msg.author.mention}')

    # Add user to a ping group
    elif msg.content.startswith(CMD['join_group']):
        group_to_join = msg.content[len(CMD['join_group'])+1:]
        await join_group(msg.author, group_to_join, msg.channel)
    # Remove user from a ping group
    elif msg.content.startswith(CMD['leave_group']):
        group_to_leave = msg.content[len(CMD['leave_group'])+1:]
        await leave_group(msg.author, group_to_leave, msg.channel)
    # List all groups a user has joined
    elif msg.content.startswith(CMD['list_groups']):
        await list_groups(msg.author, msg.channel)
    # Ping a group
    elif msg.content.startswith(CMD['send_ping']):
        # Fetch group name from message
        group_to_ping = msg.content[len(CMD['send_ping'])+1:]
        await send_ping(msg.author, group_to_ping, msg.channel)       


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


def save_data_on_file():
    """Saves the 'data' object to the data file."""
    # TODO: Don't rewrite everything every time
    with open(DATA_FILE_PATH, 'w') as data_file:
        data_file.write(json.dumps(data, sort_keys=True, indent=4))


async def join_group(user, group_to_join, channel):
    """Adds a user to a ping group"""
    # Check if group exists
    if group_to_join not in data['pings'].keys():
        await channel.send('The requested ping does not exist.')
        return

    # Check if not already on group
    if user.id in data['pings'][group_to_join]:
        await channel.send('You already joined this ping group.')
        return

    # Add user to group
    data['pings'][group_to_join].append(user.id)
    # Save changes on file
    save_data_on_file()
    # Send feedback message
    await channel.send(f'Joined the ping group `{group_to_join}`.')


async def leave_group(user, group_to_leave, channel):
    """Removes a user from a ping group"""
    # Check if group exists
    if group_to_leave not in data['pings'].keys():
        await channel.send('The requested ping does not exist.')
        return

    # Check if on group
    if user.id not in data['pings'][group_to_leave]:
        await channel.send('You are not in this ping group.')
        return

    # Remove user from group
    data['pings'][group_to_leave].remove(user.id)
    # Save changes on file
    save_data_on_file()
    # Send feedback message
    await channel.send(f'Left the ping group `{group_to_leave}`.')


async def list_groups(user, channel):
    """List all ping groups a user has joined"""
    groups_joined = []
    for group in data['pings']:
        if user.id in data['pings'][group]:
            groups_joined.append(group)

    # If user hasn't joined any groups
    if not groups_joined:
        await channel.send("You haven't joined any ping groups. To join a group, write `pb!add <group name>`")
        return

    groups_str = ''
    for group in groups_joined:
        groups_str += f'`{group}`, '
    groups_str = groups_str[:-2]
    await channel.send(f"You are part of the following ping groups: {groups_str}")


async def send_ping(author, group_to_ping, channel):
    """Sends a ping to all users of a group"""
    # Check if group exists
    if group_to_ping not in data['pings'].keys():
        await channel.send('The requested ping does not exist.')
        return

    # Send feedback message
    feedback_msg = await channel.send(f'Pinging `{group_to_ping}`.')

    # Send pings
    # If DMing everyone separately
    if DM_OR_MENTION == 'dm':
        for user_id in data['pings'][group_to_ping]:
            user = client.get_user(user_id)
            await user.send(
                f">>> {author.mention} pinged `{group_to_ping}`.{chr(10)}"
                # TODO: Make it a nice link, don't know how
                f"Click the following link to see: {feedback_msg.jump_url}{chr(10)}"
            )
    # If mentioning everyone on chat
    elif DM_OR_MENTION == 'mention':
        msg = ''
        for user_id in data['pings'][group_to_ping]:
            msg += client.get_user(user_id).mention
            msg += ', '
        msg = msg[:-2]
        await channel.send(msg)

    # Edit message to show it's done
    await feedback_msg.edit(content=f'Pinged `{group_to_ping}`.')


if __name__ == '__main__':
    with open(DATA_FILE_PATH, 'r') as data_file:
        data = json.load(data_file)
    client.run(TOKEN)
